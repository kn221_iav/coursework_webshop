const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function (app) {
  app.use(
    '/api', // Change this to match your API route
    createProxyMiddleware({
      target: 'http://localhost:5000', // Change this to your backend server URL
      changeOrigin: true,
    })
  );
};


